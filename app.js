var net = require('net');
var request = require('request');
var PORT = 4103;
var socketIOClient = require('socket.io-client');
var sailsIOClient = require('sails.io.js');

//instancia de cliente soket
//pasarle el cliente de soketio
var io = sailsIOClient(socketIOClient);

//settea la url
io.sails.url = 'http://localhost:1337';

//me subscribo al webSocket
io.socket.get('/messagetotracker/suscribe');
var sendMessage;

//escucho los eventos create en el web socket
io.socket.on("messagetotracker", function(obj) {
    console.log(obj);
    if(obj.verb === 'created'){
        sendMessage(obj.data.imei,obj.data.command,obj.data.param1);          
    }
});


net.createServer(function(sock) {
    
    // We have a connection - a socket object is assigned to the connection automatically
    console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);
    
    // Add a 'data' event handler to this instance of socket
    sock.on('data', function(data) {
        //comprueba si el mensaje proviene de un tracker
        //emprolijar esto!!!
        var arrDatos = data.toString().split(",");
        console.log (arrDatos[3]);
        if(data.toString().substring(0,2)==="$$" &&
            data.toString().substring(2,3)>="A"  &&
            data.toString().substring(2,3)<="z" &&
            arrDatos[2].toString() == 'AAA'){
            
            var dataStr = data.toString();
            var firstSeparatorI = dataStr.indexOf(",");
            largoMsj = dataStr.substring(3,firstSeparatorI);
            console.log('Largo msj: '  + largoMsj);
            dataStr = dataStr.substring(firstSeparatorI+1,dataStr.length);
            arrDatos = dataStr.toString().split(",");
            //console.log(dataStr.substring(largoMsj+1,dataStr.length).length);

            
            var gpsDate = arrDatos[5].toString();
            //pharser para la fecha
            var yyyy='20' + gpsDate.substring(0,2);
            var mm=gpsDate.substring(2,4);
            var dd=gpsDate.substring(4,6);
            var hh=gpsDate.substring(6,8);
            var mi=gpsDate.substring(8,10);
            var ss=gpsDate.substring(10,12);
            console.log('Mes' + mm);
            //pharser para entradas

            var io = new Array(15);
            
            var n = arrDatos[16]    
            var binary = parseInt(n,16).toString(2);
            for (var i = 0, len = binary.length; i < len; i++) {
                if (binary[i]==0){
                    io[i] = false; 
                }else{
                    io[i] = true;   
                }
            }

            gpsDate = new Date(yyyy,mm-1,dd,hh,mi,ss);
            request({
                url: 'http://localhost:1337/Report', //URL to hit
                method: 'POST',
               json:{
                imei: arrDatos[0],
                length:largoMsj,
                typeMsj:arrDatos[1],
                lat:arrDatos[3],
                lng:arrDatos[4],
                alt:arrDatos[12],
                gpsTime:gpsDate,
                gpsStat:arrDatos[6],
                numSat:arrDatos[7],
                gprsSign:arrDatos[8],
                speed:arrDatos[9],
                heding:arrDatos[10],
                presicion:arrDatos[11],
                odo:arrDatos[13],
                runTime:arrDatos[14],
                baseId:arrDatos[15],
                dIn1:io[8],
                dIn2:io[9],
                dIn3:io[10],
                dIn4:io[11],
                dIn5:io[12],
                dIn6:io[13],
                dIn7:io[14],
                dIn8:io[15],
                dOut1:io[0],
                dOut2:io[1],
                dOut3:io[2],
                dOut4:io[3],
                dOut5:io[4],
                dOut6:io[5],
                dOut7:io[6],
                dOut8:io[7]/*,
                Ad1:{
                    type:'integer'
                },
                Ad2:{
                    type:'integer'
                },
                Ad3:{
                    type:'integer'
                },
                batery:{
                    type:'integer'
                },
                externalPower:{
                    type:'integer'
                },
                tracker:{
                    model:'tracker'
                },
                ipClient:{
                  type:'string',
                  size:15
                },
                portClient:{
                  type:'string',
                  size:5
                }*/
                
               }
            }, function(error, response, body){
                if(error) {
                    console.log(error);
                } else {
                    console.log(response.statusCode, body);
                }
            });
            
        }else{
            console.log('Mensaje descartado');
        }
        //console.log('DATA ' + sock.remoteAddress + ': ' + data);
        // Write the data back to the socket, the client will receive it as data from the server
        //sock.write('You said "' + data + '"');
        
    });
    
    // Add a 'close' event handler to this instance of socket
    sock.on('end', function(data) {
        console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
    });

    sendMessage = function (imei,command,param1){
        switch (command){
            case 'C01':
                var msj= imei.toString() + ',' + command.toString() + ',0' + ',' + stringToBool(param1) + '2222*' ;
                msj = '@@M' + (msj.length+5) + ',' + msj;
                msj += checksum(msj) + String.fromCharCode(13) + String.fromCharCode(10); 
                sock.write(msj); 
                console.log(msj);
            //sock.write('@@M33,011691003111617,C01,0,12222*D0' + String.fromCharCode(13) + String.fromCharCode(10));
        }
    };
    
}).listen(PORT);

function checksum(message){
    var sum=0;
    message.toString().split('').forEach(function(cr){
        sum += cr.charCodeAt(0);
        console.log (cr + ' ' + cr.charCodeAt(0));
    });

    //sum = sum % 256;
    console.log('la suma es ' + sum);
    sum = sum.toString(16);
    console.log('En hex:' + sum);
    sum = sum.toString().substring(1,3);
    
    console.log('Devuelve' + sum);
    return sum;
};

function stringToBool(param){
    param = param.toString();
    if (param == 'true'){
        return 1;
    }
    else{
        return 0;
    }
}

 /*function stringToHex(stringValue ){
        var binaryValue = stringToBinary( stringValue );
        var hexValue = binaryEncode( binaryValue, "hex" );
        return( lcase( hexValue ) );
};

function stringToBinary(stringValue ){
    var base64Value = toBase64( stringValue );
    var binaryValue = toBinary( base64Value );
    return( binaryValue );
};*/

console.log('Server listening on '+ PORT);